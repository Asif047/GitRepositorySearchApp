package com.example.gitrepositorrysearchapp.dao;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;

import com.example.gitrepositorrysearchapp.model.Repository;
import com.example.gitrepositorrysearchapp.model.dto.RepositoryDto;

import java.util.List;

@Dao
public interface RepositoryDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insert(List<RepositoryDto> repositoryDtoList);

    @Query("SELECT * FROM repository")
    LiveData<List<RepositoryDto>> getAllRepositories();

    @Query("DELETE FROM repository")
    void deleteAll();

}
