package com.example.gitrepositorrysearchapp;

import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;
import android.util.Log;
import android.widget.AbsListView;

import com.example.gitrepositorrysearchapp.adapter.RepositoryAdapter;
import com.example.gitrepositorrysearchapp.model.Owner;
import com.example.gitrepositorrysearchapp.model.Repository;
import com.example.gitrepositorrysearchapp.model.dto.RepositoryDto;
import com.example.gitrepositorrysearchapp.network.Retrofit;
import com.example.gitrepositorrysearchapp.pagination.PagedList;
import com.example.gitrepositorrysearchapp.repository.GitRepoRepository;
import com.example.gitrepositorrysearchapp.view_model.RepositoryViewModel;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class MainActivity extends AppCompatActivity {

    private RepositoryViewModel repositoryViewModel;
    private RecyclerView recyclerView;
    private Repository repository;
    private List<RepositoryDto> repositoryDtoList;
    private List<Owner> ownerList;
    private GitRepoRepository gitRepoRepository;
    private RepositoryAdapter repositoryAdapter;

    //pagination work starts
    Boolean isScrolling = false;
    int currentItems, totalItem, scrollOutItems = 0;
    int pageno = 1;
    int page_size = 10;
    //pagination work ends

    private LinearLayoutManager layoutManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        ownerList = new ArrayList<>();
        layoutManager = new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false);

        recyclerView = findViewById(R.id.recyclerView);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerView.setItemAnimator(null);

        gitRepoRepository = new GitRepoRepository(getApplication());
        repository = new Repository();
        repositoryDtoList = new ArrayList<>();
        repositoryAdapter = new RepositoryAdapter(this, repositoryDtoList, ownerList);
        repositoryViewModel = new ViewModelProvider(this).get(RepositoryViewModel.class);
        networkRequest();


        repositoryViewModel.getAllRepositories().observe(this, new Observer<List<RepositoryDto>>() {
            @Override
            public void onChanged(List<RepositoryDto> repositoryDtoList) {


                recyclerView.setAdapter(repositoryAdapter);
                repositoryAdapter.getAllRepos(repositoryDtoList);
                //Log.d("##MAIN:", "on changed:"+repositoryDtoList);

            }
        });

        recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);

                if (newState == AbsListView.OnScrollListener.SCROLL_STATE_TOUCH_SCROLL) {
                    isScrolling = true;
                }

            }

            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);

                currentItems = layoutManager.getChildCount();

                if(((LinearLayoutManager)recyclerView.getLayoutManager())
                        .findLastVisibleItemPosition() > scrollOutItems) {
                    scrollOutItems = ((LinearLayoutManager)recyclerView.getLayoutManager())
                            .findLastVisibleItemPosition();
                }

                if (isScrolling && (scrollOutItems >= page_size - 1) &&
                        pageno <= (totalItem / 10)) {

                    isScrolling = false;
                    pageno++;
                    networkRequest();
                }
            }
        });

        //pagination work ends


    }

    private void networkRequest() {
        //Log.e("I_AM_CALLED", "YES");
        page_size = page_size + 25;
        Retrofit retrofit = new Retrofit();
        Call<Repository> call = retrofit.api.getAllRepositories("Android", pageno, page_size);

        call.enqueue(new Callback<Repository>() {
            @Override
            public void onResponse(Call<Repository> call, Response<Repository> response) {
                if(response.isSuccessful()) {
                    repositoryDtoList.clear();
                    //Log.e("###RES:",response.body().getItems().get(0).getName());
                    totalItem = response.body().getTotalCount();

                    for(int i = 0; i < response.body().getItems().size(); i++) {
                        RepositoryDto repositoryDto = new RepositoryDto(
                                response.body().getItems().get(i).getId(),
                                response.body().getItems().get(i).getNodeId(),
                                response.body().getItems().get(i).getName(),
                                response.body().getItems().get(i).getFullName(),
                                response.body().getItems().get(i).getPrivate()
                        );
                        repositoryDtoList.add(repositoryDto);

                        ownerList.add(response.body().getItems().get(i).getOwner());
                    }

                    gitRepoRepository.insert(repositoryDtoList);

                    repositoryAdapter.notifyDataSetChanged();
                    if(scrollOutItems >= 10)
                    recyclerView.scrollToPosition(scrollOutItems);

                }
            }

            @Override
            public void onFailure(Call<Repository> call, Throwable t) {
                //Log.e("###RES2:",t.toString());
            }
        });
    }

}