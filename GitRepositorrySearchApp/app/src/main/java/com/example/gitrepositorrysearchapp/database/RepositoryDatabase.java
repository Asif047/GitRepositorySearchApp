package com.example.gitrepositorrysearchapp.database;

import android.content.Context;
import android.os.AsyncTask;

import androidx.annotation.NonNull;
import androidx.room.Database;
import androidx.room.Room;
import androidx.room.RoomDatabase;
import androidx.sqlite.db.SupportSQLiteDatabase;

import com.example.gitrepositorrysearchapp.dao.RepositoryDao;
import com.example.gitrepositorrysearchapp.model.dto.RepositoryDto;



@Database(entities = {RepositoryDto.class}, version = 2)
public abstract class RepositoryDatabase extends RoomDatabase{

    private static final String DATABASE_NAME = "RepositoryDatabase";
    public abstract RepositoryDao repositoryDao();

    public static volatile RepositoryDatabase INSTANCE;

    public static RepositoryDatabase getInstance(Context context) {
        if(INSTANCE == null) {
            synchronized (RepositoryDatabase.class) {
                if(INSTANCE == null) {
                    INSTANCE = Room.databaseBuilder(context,
                            RepositoryDatabase.class,
                            DATABASE_NAME
                            ).addCallback(callback)
                            .fallbackToDestructiveMigration()
                            .build();
                }
            }
        }
        return INSTANCE;
    }



    static Callback callback=new Callback() {
        @Override
        public void onCreate(@NonNull SupportSQLiteDatabase db) {
            super.onCreate(db);
            new PopulateAsyncTask(INSTANCE);
        }
    };
    static class PopulateAsyncTask extends AsyncTask<Void,Void,Void>
    {
        private RepositoryDao repositoryDao;
        PopulateAsyncTask(RepositoryDatabase repositoryDatabase)
        {
            repositoryDao = repositoryDatabase.repositoryDao();
        }

        @Override
        protected Void doInBackground(Void... voids) {
            repositoryDao.deleteAll();
            return null;
        }
    }


}
