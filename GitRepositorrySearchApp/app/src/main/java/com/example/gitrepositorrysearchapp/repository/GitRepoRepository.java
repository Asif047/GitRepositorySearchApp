package com.example.gitrepositorrysearchapp.repository;

import android.app.Application;
import android.os.AsyncTask;

import androidx.lifecycle.LiveData;

import com.example.gitrepositorrysearchapp.dao.RepositoryDao;
import com.example.gitrepositorrysearchapp.database.RepositoryDatabase;
import com.example.gitrepositorrysearchapp.model.Repository;
import com.example.gitrepositorrysearchapp.model.dto.RepositoryDto;

import java.util.List;

public class GitRepoRepository {

    private RepositoryDatabase database;
    private LiveData<List<RepositoryDto>> getAllRepositories;

    public GitRepoRepository(Application application) {
        database = RepositoryDatabase.getInstance(application);
        getAllRepositories = database.repositoryDao().getAllRepositories();
    }

    public  void insert(List<RepositoryDto> repositoryDtoList) {
        new InsertAsynTask(database).execute(repositoryDtoList);
    }

    public LiveData<List<RepositoryDto>> getAllRepositories()
    {
        return getAllRepositories;
    }

    static class InsertAsynTask extends AsyncTask<List<RepositoryDto>,Void,Void> {
        private RepositoryDao repositoryDao;
        InsertAsynTask(RepositoryDatabase universityDatabase)
        {
            repositoryDao = universityDatabase.repositoryDao();
        }
        @Override
        protected Void doInBackground(List<RepositoryDto>... lists) {
            repositoryDao.insert(lists[0]);
            return null;
        }
    }


}
