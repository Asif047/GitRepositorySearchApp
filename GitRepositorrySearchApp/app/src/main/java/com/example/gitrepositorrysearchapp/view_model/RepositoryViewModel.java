package com.example.gitrepositorrysearchapp.view_model;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;

import com.example.gitrepositorrysearchapp.model.dto.RepositoryDto;
import com.example.gitrepositorrysearchapp.repository.GitRepoRepository;

import java.util.List;

public class RepositoryViewModel extends AndroidViewModel {

    private GitRepoRepository gitRepoRepository;
    private LiveData<List<RepositoryDto>> getAllRepository;

    public RepositoryViewModel(@NonNull Application application) {
        super(application);
        gitRepoRepository = new GitRepoRepository(application);
        getAllRepository = gitRepoRepository.getAllRepositories();
    }

    public void insert(List<RepositoryDto> list){
        gitRepoRepository.insert(list);
    }

    public LiveData<List<RepositoryDto>> getAllRepositories() {
        return getAllRepository;
    }
}
