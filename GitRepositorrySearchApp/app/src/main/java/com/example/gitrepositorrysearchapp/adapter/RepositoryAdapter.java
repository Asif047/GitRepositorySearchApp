package com.example.gitrepositorrysearchapp.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.gitrepositorrysearchapp.R;
import com.example.gitrepositorrysearchapp.model.Owner;
import com.example.gitrepositorrysearchapp.model.dto.RepositoryDto;

import java.util.List;

public class RepositoryAdapter extends RecyclerView.Adapter<RepositoryAdapter.MyViewHolder> {

    private Context context;
    private List<RepositoryDto> repositoryDtoList;
    private List<Owner> ownerList;

    public RepositoryAdapter(Context context, List<RepositoryDto> repositoryDtoList, List<Owner> ownerList) {
        this.context = context;
        this.repositoryDtoList = repositoryDtoList;
        this.ownerList = ownerList;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new MyViewHolder(LayoutInflater.from(parent.getContext())
                .inflate(R.layout.row_item_repository, parent, false));

    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {
        RepositoryDto repositoryDto = repositoryDtoList.get(position);
        holder.tvRepoName.setText(repositoryDto.getFullName()+"");
    }

    public void getAllRepos(List<RepositoryDto> repositoryDtoList) {
        this.repositoryDtoList = repositoryDtoList;
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }

    @Override
    public int getItemCount() {
        return repositoryDtoList.size();
    }

    public static  class MyViewHolder extends  RecyclerView.ViewHolder{
        public TextView tvRepoName;

        public MyViewHolder(@NonNull View itemView) {
            super(itemView);
            tvRepoName = itemView.findViewById(R.id.tvRepoName);
        }
    }

}
