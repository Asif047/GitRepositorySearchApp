package com.example.gitrepositorrysearchapp.network;


import com.example.gitrepositorrysearchapp.model.Repository;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface Api {

    @GET("search/repositories")
    Call<Repository> getAllRepositories(@Query("q") String q, @Query("page") int page, @Query("per_page") int per_page);

}
